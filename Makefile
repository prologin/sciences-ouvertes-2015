all:
	pandoc jour_1_matin.md -o jour_1_matin.pdf
	pandoc jour_1_aprem.md -o jour_1_aprem.pdf
	pandoc jour_2.md -o jour_2.pdf
	pandoc jour_3.md -o jour_3.pdf
	pandoc jour_4.md -o jour_4.pdf

clean:
	rm -f *.pdf
