% Stage Programmation et Algorithmes
% Sciences Ouvertes — Prologin
% 28 Décembre 2015

\newpage

# Dictionnaires et dictionnaire

Commencez par télécharger un dictionnaire de la langue 
française\footnote{\url{http://hatrix.fr/so/dictionnaire.txt}} puis chargez le dans
votre code :

```python
    dico = open('dictionnaire.txt').read().splitlines()
```

Le résultat est alors un tableau contenant tous les mots.

## Exercices

* Combien de mots contient ce dictionnaire ?
* Combien de mots font exactement 15 caractères ?
* Quels sont les mots contenant la lettre « ù » ?
* Combien de mots finissent par la lettre « q » ?


\newpage

# Liste de films

Le top100 des films visionnés au États-Unis sur la plateforme iTunes peut être
téléchargé ici \footnote{\url{http://hatrix.fr/so/films.json}}.\
Le format json est semblable aux dictionnaires python.

```python
    import json

    brut = json.load(open(films.json))
    top = brut['feed']['entry'] # liste des films
```

## Exercices

* Afficher le top10 des films sous cette forme :

```
    1 Mission: Impossible - Rogue Nation
    2 …
    …
    10 …
```
* Quel est le classement du film « Gravity » ?
* Quel est le réalisateur du film « The LEGO Movie » ?
* Combien de films sont sortis avant 2000 ?
* Quel est le film le plus récent ? Le plus vieux ?
* Quelle est la catégorie de films la plus représentée ?
* Quel est le réalisateur le plus présent dans le top100 ?
* Combien cela coûterait-il d'acheter le top10 sur iTunes ? de le louer ?
* Quel est le mois ayant vu le plus de sorties au cinéma ?
* Quels sont les 10 meilleurs films à voir en ayant un budget limité ?


